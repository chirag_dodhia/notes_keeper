class CreateSharedNotes < ActiveRecord::Migration[6.0]
  def change
    create_table :shared_notes do |t|
      t.string :email
      t.integer :note_id
      t.string :role

      t.timestamps
    end
  end
end
