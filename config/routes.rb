Rails.application.routes.draw do
  resources :notes
  get "note/:id/share" => "notes#share", :as => "share_note"
  root "application#welcome", :as => "home", via: :all
  post "note/shared" => "notes#shared", :as => "shared_note_update"
  delete "delete_note" => "notes#destroy", :as => "delete_note"
  get "signup" => "users#new", :as => "user_signup"
  post "user_create" => "users#create", :as => "user_create"
  controller :sessions do
    get 'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
  end

end
