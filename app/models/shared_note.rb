class SharedNote < ApplicationRecord
  belongs_to :note
  validates :email, presence: true
  validates :role, presence: true
end
