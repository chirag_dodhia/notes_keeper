class ApplicationController < ActionController::Base
  before_action :authorize
  skip_before_action :authorize, only: [:welcome]

  def welcome
    if session[:user_id].present?
      redirect_to notes_path
    else
      render "layouts/welcome"
    end
  end

  protected
	def authorize
    unless User.find_by(id: session[:user_id])
      redirect_to login_url, notice: "Please Log in"
    end
  end
end
