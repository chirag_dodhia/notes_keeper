class NotesController < ApplicationController
  before_action :set_note, only: [:show, :edit, :update, :destroy]
  skip_before_action :authorize, only: [:share, :shared]
  before_action :owner_access, only: [:destroy, :share]
  before_action :update_access, only: [:edit]
  before_action :read_access, only: [:show]

  def index
    @notes = Note.where(user_id: session[:user_id])
    email = User.find(session[:user_id]).email
    cn = SharedNote.where("email = ? AND role = ?", email, "Contributor")
    cn_ids = cn.pluck(:note_id) if cn.present?
    @contributer_access_notes = Note.where(id: cn_ids)
    rn = SharedNote.where("email = ? AND role = ?", email, "Reader")
    rn_ids = rn.pluck(:note_id) if rn.present?
    @reader_access_notes = Note.where(id: rn_ids)
  end

  def show
    @owner_access = is_owner?
    @update_access = @owner_access || can_update?
  end

  def new
    @note = Note.new
  end

  def share
    @note_id = params[:id].to_i
  end

  def shared
    if note = SharedNote.where("email = ? AND note_id = ?", params[:email], params[:note_id]).first
      note.role = params[:role]
      note.save
    else
      shared_note = SharedNote.new
      shared_note.email = params[:email]
      shared_note.note_id = params[:note_id]
      shared_note.role = params[:role]
      shared_note.save
    end
    redirect_to notes_url, notice: 'Note was successfully shared.'
  end

  def create
    @note = Note.new(note_params)
    @note.user_id = session[:user_id]

    respond_to do |format|
      if @note.save
        format.html { redirect_to @note, notice: 'Note was successfully created.' }
        format.json { render :show, status: :created, location: @note }
      else
        format.html { render :new }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @note.update(note_params)
        format.html { redirect_to @note, notice: 'Note was successfully updated.' }
        format.json { render :show, status: :ok, location: @note }
      else
        format.html { render :edit }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @note.destroy
    respond_to do |format|
      format.html { redirect_to notes_url, notice: 'Note was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def owner_access
    redirect_to notes_url, notice: "You are not authorized for this operation" unless is_owner?
  end

  def update_access
    if !is_owner? && !can_update?
      redirect_to notes_url, notice: "You are not authorized for this operation"
    end
  end

  def read_access
    unless is_owner?
      email = User.find_by_id(session[:user_id]).email
      shared_note = SharedNote.where(note_id: params[:id], email: email).first
      redirect_to notes_url, notice: "You are not authorized for this operation" unless shared_note.present? && (shared_note.role.downcase == "contributor" || shared_note.role.downcase == "reader")
    end
  end

  def is_owner?
    Note.find(params[:id]).user_id == session[:user_id]
  end

  def can_update?
    email = User.find_by_id(session[:user_id]).email
    shared_note = SharedNote.where(note_id: params[:id], email: email).first
    shared_note.present? && shared_note.role.downcase == "contributor"
  end

  private
    def set_note
      @note = Note.find(params[:id])
    end

    def note_params
      params.require(:note).permit(:title, :description, :user_id)
    end
end
