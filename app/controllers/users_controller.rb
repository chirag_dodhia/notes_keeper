class UsersController < ApplicationController
  skip_before_action :authorize

  def show
  end

  def new
    @user = User.new
    @hide_sign_up = true
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        session[:user_id] = @user.id
        format.html { redirect_to notes_url, notice: 'Welcome to Notes Share' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors,
          status: :unprocessable_entity }
      end
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :password, :email, :password_confirmation)
    end
end
